package br.com.userapp.ui.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.userapp.R
import br.com.userapp.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel : MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareRecycleView()
        initObservers()

        viewModel.getUsers()
    }

    private fun initObservers(){
        viewModel.usersResult.observe(this,{
            rv_users.adapter = UsersAdapter(it ?: emptyList())
        })
    }

    private fun prepareRecycleView(){
        rv_users.layoutManager = LinearLayoutManager(this)
    }
}