package br.com.userapp.ui.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.userapp.R
import br.com.userapp.model.Users
import coil.api.load

class UsersAdapter (private val users: List<Users>):RecyclerView.Adapter<UsersAdapter.UsersViewHolder>(){

    inner class UsersViewHolder(view: View) : RecyclerView.ViewHolder(view){

        val tvName = view.findViewById<TextView>(R.id.tv_name)
        val tvEmail = view.findViewById<TextView>(R.id.tv_email)
        val ivPhoto = view.findViewById<ImageView>(R.id.iv_photo)

        fun bindUsers(user: Users){
            tvName.text = "${user.firstName} ${user.lastName}"
            tvEmail.text = user.email
            ivPhoto.load(user.avatar){
                crossfade(true)
                crossfade(300)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder (
            LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false)
        )
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.bindUsers(users[position])
    }

    override fun getItemCount(): Int = users.size
}

