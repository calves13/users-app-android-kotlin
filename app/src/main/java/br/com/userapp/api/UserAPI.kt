package br.com.userapp.api

import br.com.userapp.model.UsersResponse
import retrofit2.Call
import retrofit2.http.GET

interface UserAPI {
    @GET("/api/users")
    fun getUsers():Call<UsersResponse>
}