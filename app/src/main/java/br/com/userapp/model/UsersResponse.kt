package br.com.userapp.model

import com.google.gson.annotations.SerializedName

data class UsersResponse(
    val data: List<Users>
)

data class Users(
    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,
    val email: String,
    val avatar: String
)