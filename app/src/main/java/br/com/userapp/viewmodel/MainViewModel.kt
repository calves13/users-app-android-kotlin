package br.com.userapp.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.userapp.api.UserAPI
import br.com.userapp.api.UsersApiService
import br.com.userapp.model.Users
import br.com.userapp.model.UsersResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    val usersResult = MutableLiveData<List<Users>>()

    fun getUsers(){
        val usersService = UsersApiService.getInstance().create(UserAPI::class.java)
        usersService.getUsers().enqueue(object : Callback<UsersResponse>{
            override fun onResponse(call: Call<UsersResponse>, response: Response<UsersResponse>) {
                resultUsersSuccess(response.body()?.data)
            }

            override fun onFailure(call: Call<UsersResponse>, t: Throwable) {
                Log.v("Erro", ">>>>>>>>>> ${t.message}")
            }
        })
    }

    fun resultUsersSuccess(results: List<Users>?){
        usersResult.postValue(results ?: emptyList())
    }
}