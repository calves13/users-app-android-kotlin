**Users App - projeto Android + Kotlin
---

## Arquiteura e Bibliotecas

Aqui você encontrará todas as bibliotecas utilizadas no projeto.

1. Refrofit - Permite usar API externas no App.
2. Coil - Nova biblioteca desenvolvida em Kotlin para carregamento de Images.
3. MVVM - Seguindo o padrao de projeto que o Google recomenda hoje em dia.

---

## Telas de Exemplo  de Usuários em funcionamento


Tela de Lista de Usuários
![Scheme](images/lista.jpg)



